package com.mehuljoisar.gsontojsondemo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        MyEntity example = new MyEntity();
        example.setName("Mr. Spock");
        example.setRace("Vulcan");
        example.setSex("male");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String flat = gson.toJson(example);
        ((EditText)findViewById(R.id.edit)).setText(flat);
    }
}